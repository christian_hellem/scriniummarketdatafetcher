﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArrayList = System.Collections.ArrayList;
using Datetime = Bloomberglp.Blpapi.Datetime;
using Event = Bloomberglp.Blpapi.Event;
using Element = Bloomberglp.Blpapi.Element;
using Message = Bloomberglp.Blpapi.Message;
using Name = Bloomberglp.Blpapi.Name;
using Identity = Bloomberglp.Blpapi.Identity;
using CorrelationID = Bloomberglp.Blpapi.CorrelationID;
using EventQueue = Bloomberglp.Blpapi.EventQueue;
using Request = Bloomberglp.Blpapi.Request;
using Service = Bloomberglp.Blpapi.Service;
using Session = Bloomberglp.Blpapi.Session;
using SessionOptions = Bloomberglp.Blpapi.SessionOptions;
using DataType = Bloomberglp.Blpapi.Schema.Datatype;
using SeatType = Bloomberglp.Blpapi.SeatType;
using ScriniumMarketDataBLL;

namespace ScriniumConsoleTester
{
    class Program
    {
        static void Main(string[] args)
        {
            var fetcher = new ScriniumMarketDataFetcher.MarketDataFetcther();
            fetcher.SelectedFields.Add("PX_LAST");
            fetcher.SelectedStartDate = DateTime.Today.AddDays(-10);
            fetcher.SelectedEndDate = DateTime.Today;
            fetcher.SelectedSecurities.Add("MAERSKB DC Equity");
            
            fetcher.FetchHistoricData();
            Console.ReadKey();
        }
    }
}
