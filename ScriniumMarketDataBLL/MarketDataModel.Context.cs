﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ScriniumMarketDataBLL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ScriniumMarketDataEntities : DbContext
    {
        public ScriniumMarketDataEntities()
            : base("name=ScriniumMarketDataEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<SecurityMarketData> SecurityMarketData { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
    }
}
