﻿USE [ScriniumMarketData]
GO

/****** Object:  Table [dbo].[SecurityMarketData]    Script Date: 18-07-2017 18:48:09 ******/
DROP TABLE [dbo].[SecurityMarketData]
GO

/****** Object:  Table [dbo].[SecurityMarketData]    Script Date: 18-07-2017 18:48:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SecurityMarketData](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Security] [nvarchar](50) NULL,
	[Desc] [nvarchar](50) NULL,
	[CustomerID] [int] NULL,
	[SOURCE] [nvarchar](50) NULL,
	[DATE] [datetime] NULL,
	[PX_LAST] [decimal](18, 0) NULL,
	[HIGH] [decimal](18, 0) NULL,
	[LOW] [decimal](18, 0) NULL,
	[OPEN] [decimal](18, 0) NULL,
	[PREV_SES_LAST_PRICE] [decimal](18, 0) NULL,
	[LAST_TRADE] [decimal](18, 0) NULL,
 CONSTRAINT [PK_SecurityMarketData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


