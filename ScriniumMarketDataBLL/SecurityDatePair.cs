﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScriniumMarketDataBLL
{
    public class SecurityDatePair
    {
        public int CUSTOMER { get; set; }
        public DateTime? DATE { get; set; }
        public string SEC { get; set; }
    }
}
