﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArrayList = System.Collections.ArrayList;
using Datetime = Bloomberglp.Blpapi.Datetime;
using Event = Bloomberglp.Blpapi.Event;
using Element = Bloomberglp.Blpapi.Element;
using Message = Bloomberglp.Blpapi.Message;
using Name = Bloomberglp.Blpapi.Name;
using Identity = Bloomberglp.Blpapi.Identity;
using CorrelationID = Bloomberglp.Blpapi.CorrelationID;
using EventQueue = Bloomberglp.Blpapi.EventQueue;
using Request = Bloomberglp.Blpapi.Request;
using Service = Bloomberglp.Blpapi.Service;
using Session = Bloomberglp.Blpapi.Session;
using SessionOptions = Bloomberglp.Blpapi.SessionOptions;
using DataType = Bloomberglp.Blpapi.Schema.Datatype;
using SeatType = Bloomberglp.Blpapi.SeatType;
using ScriniumMarketDataBLL;

namespace ScriniumMarketDataFetcher
{
    public partial class MarketDataFetcther
    {
        public void FetchEndOfDayPricesForCustomer(int customerId, DateTime tradedate)
        {
            switch (d_identity.SeatType)
            {
                case SeatType.BPS:
                    // send request
                    sendHistoricDataRequest();
                    // wait for events from session.
                    eventLoop();
                    break;
                default:
                    throw new Exception("User is invalid.");
            }
        }

        private void eventLoop()
        {
            //run through all events expected - signified by a "RESPONSE" event
            bool done = false;
            int secNbr = 0;
            while (!done)
            {
                Event eventObj = d_session.NextEvent();
                if (eventObj.Type == Event.EventType.PARTIAL_RESPONSE)
                {
                    System.Console.WriteLine(""); System.Console.WriteLine("");
                    System.Console.WriteLine("Processing Security " + d_securities[secNbr]);
                    secNbr++;
                    processResponseEvent(eventObj);
                }
                else if (eventObj.Type == Event.EventType.RESPONSE)
                {
                    System.Console.WriteLine(""); System.Console.WriteLine("");
                    System.Console.WriteLine("Processing Security " + d_securities[secNbr]);
                    processResponseEvent(eventObj);
                    done = true;
                }
                else
                {
                    foreach (Message msg in eventObj)
                    {
                        System.Console.WriteLine(msg.AsElement);
                        if (eventObj.Type == Event.EventType.SESSION_STATUS)
                        {
                            if (msg.MessageType.Equals("SessionTerminated"))
                            {
                                done = true;
                            }
                        }
                    }
                }
            }
        }//end eventLoop
        
        private void processResponseEvent(Event eventObj)
        {
            foreach (Message msg in eventObj)
            {
                if (msg.HasElement(RESPONSE_ERROR))
                {
                    printErrorInfo("REQUEST FAILED: ", msg.GetElement(RESPONSE_ERROR));
                    continue;
                }
                if ((eventObj.Type != Event.EventType.PARTIAL_RESPONSE) && (eventObj.Type != Event.EventType.RESPONSE))
                {
                    continue;
                }
                if (!ProcessErrors(msg))
                {
                    ProcessExceptions(msg);
                    ProcessFields(msg);
                }
            }
        }//end processResponseEvent

        private void printErrorInfo(string leadingStr, Element errorInfo)
        {
            System.Console.WriteLine(leadingStr +
                                     errorInfo.GetElementAsString(CATEGORY) + " (" +
                                     errorInfo.GetElementAsString(MESSAGE) + ")");
        }//end printErrorInfo

        bool ProcessExceptions(Message msg)
        {
            Element securityData = msg.GetElement(SECURITY_DATA);
            Element field_exceptions = securityData.GetElement(FIELD_EXCEPTIONS);

            if (field_exceptions.NumValues > 0)
            {
                Element element = field_exceptions.GetValueAsElement(0);
                Element field_id = element.GetElement(FIELD_ID);
                Element error_info = element.GetElement(ERROR_INFO);
                Element error_message = error_info.GetElement(MESSAGE);
                System.Console.WriteLine(field_id);
                System.Console.WriteLine(error_message);
                return true;
            }
            return false;
        }

        bool ProcessErrors(Message msg)
        {
            Element securityData = msg.GetElement(SECURITY_DATA);

            if (securityData.HasElement(SECURITY_ERROR))
            {
                Element security_error = securityData.GetElement(SECURITY_ERROR);
                Element error_message = security_error.GetElement(MESSAGE);
                System.Console.WriteLine(error_message);
                return true;
            }
            return false;
        }

        void ProcessFields(Message msg)
        {
          
            String delimiter = "\t";

            //Print out the date column header
            
            System.Console.Write("DATE" + delimiter + delimiter);

            // Print out the field column headers
            for (int k = 0; k < d_fields.Count; k++)
            {
                System.Console.Write(d_fields[k].ToString() + delimiter);
            }
            System.Console.Write("\n\n");

            Element securityData = msg.GetElement(SECURITY_DATA);
            Element fieldData = securityData.GetElement(FIELD_DATA);

            //Iterate through all field values returned in the message
            if (fieldData.NumValues > 0)
            {
                for (int j = 0; j < fieldData.NumValues; j++)
                {
                    Element element = fieldData.GetValueAsElement(j);

                    //Print out the date
                    Datetime date = element.GetElementAsDatetime(DATE);
                    System.Console.WriteLine("");
                    System.Console.Write(date.DayOfMonth + "/" + date.Month + "/" + date.Year + delimiter);

                    //Check for the presence of all the fields requested
                    for (int k = 0; k < d_fields.Count; k++)
                    {
                        String temp_field_str = d_fields[k].ToString();
                        if (element.HasElement(temp_field_str))
                        {
                            Element temp_field = element.GetElement(temp_field_str);
                            Name TEMP_FIELD_STR = new Name(temp_field_str);

                            int datatype = temp_field.Datatype.GetHashCode();

                            //Extract the value dependent on the dataype and print to the console
                            switch (datatype)
                            {
                                case (int)DataType.BOOL://Bool
                                    {
                                        bool field1;
                                        field1 = element.GetElementAsBool(TEMP_FIELD_STR);
                                        System.Console.Write(field1 + delimiter);
                                        break;
                                    }
                                case (int)DataType.CHAR://Char
                                    {
                                        char field1;
                                        field1 = element.GetElementAsChar(TEMP_FIELD_STR);
                                        System.Console.Write(field1 + delimiter);
                                        break;
                                    }
                                case (int)DataType.INT32://Int32
                                    {
                                        Int32 field1;
                                        field1 = element.GetElementAsInt32(TEMP_FIELD_STR);
                                        System.Console.Write(field1 + delimiter);
                                        break;
                                    }
                                case (int)DataType.INT64://Int64
                                    {
                                        Int64 field1;
                                        field1 = element.GetElementAsInt64(TEMP_FIELD_STR);
                                        System.Console.Write(field1 + delimiter);
                                        break;
                                    }
                                case (int)DataType.FLOAT32://Float32
                                    {
                                        float field1;
                                        field1 = element.GetElementAsFloat32(TEMP_FIELD_STR);
                                        System.Console.Write(field1 + delimiter);
                                        break;
                                    }
                                case (int)DataType.FLOAT64://Float64
                                    {
                                        double field1;
                                        field1 = element.GetElementAsFloat64(TEMP_FIELD_STR);
                                        System.Console.Write(field1 + delimiter);
                                        break;
                                    }
                                case (int)DataType.STRING://String
                                    {
                                        String field1;
                                        field1 = element.GetElementAsString(TEMP_FIELD_STR);
                                        System.Console.Write(field1 + delimiter);
                                        break;
                                    }
                                case (int)DataType.DATE://Date
                                    {
                                        Datetime field1;
                                        field1 = element.GetElementAsDatetime(TEMP_FIELD_STR);
                                        System.Console.Write(field1.Year + '/' + field1.Month + '/' + field1.DayOfMonth + delimiter);
                                        break;
                                    }
                                case (int)DataType.TIME://Time
                                    {
                                        Datetime field1;
                                        field1 = element.GetElementAsDatetime(TEMP_FIELD_STR);
                                        System.Console.Write(field1.Hour + '/' + field1.Minute + '/' + field1.Second + delimiter);
                                        break;
                                    }
                                case (int)DataType.DATETIME://Datetime
                                    {
                                        Datetime field1;
                                        field1 = element.GetElementAsDatetime(TEMP_FIELD_STR);
                                        System.Console.Write(field1.Year + '/' + field1.Month + '/' + field1.DayOfMonth + '/');
                                        System.Console.Write(field1.Hour + '/' + field1.Minute + '/' + field1.Second + delimiter);
                                        break;
                                    }
                                default:
                                    {
                                        String field1;
                                        field1 = element.GetElementAsString(TEMP_FIELD_STR);
                                        System.Console.Write(field1 + delimiter);
                                        break;
                                    }
                            }
                        }
                        //System.Console.WriteLine("");
                    }
                }
            }
        }
    }
}