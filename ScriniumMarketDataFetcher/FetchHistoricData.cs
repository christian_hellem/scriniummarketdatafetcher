﻿using Bloomberglp.Blpapi;
using ScriniumMarketDataBLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataType = Bloomberglp.Blpapi.Schema.Datatype;
using SeatType = Bloomberglp.Blpapi.SeatType;

namespace ScriniumMarketDataFetcher
{
    public partial class MarketDataFetcther
    {
        public List<SecurityMarketData> FetchHistoricData(List<SecurityDatePair> securities2fetch)
        {
            throw new NotImplementedException();
        }

        public List<SecurityMarketData> FetchHistoricData(SecurityDatePair securityDatePair)
        {
            var fetchedMarketData = new List<SecurityMarketData>();

            if (!d_session.OpenService(REFDATA_SVC))
                throw new Exception("Failed to open service: " + REFDATA_SVC);

            Service fieldInfoService = d_session.GetService(REFDATA_SVC);
            Request request = fieldInfoService.CreateRequest("HistoricalDataRequest");

            // Add securities to request
            Element securities = request.GetElement("securities");
            securities.AppendValue(securityDatePair.SEC);

            Element fields = request.GetElement("fields");
            fields.AppendValue("PX_LAST");
            fields.AppendValue("HIGH");
            fields.AppendValue("LOW");
            fields.AppendValue("OPEN");
            fields.AppendValue("PREV_SES_LAST_PRICE");
            fields.AppendValue("LAST_TRADE");

            this.SelectedStartDate = securityDatePair.DATE ?? DateTime.Today;
            this.SelectedEndDate = DateTime.Today;

            request.Set("startDate", d_startDate);
            request.Set("endDate", d_endDate);

            if (d_authOption != "NONE")
                d_session.SendRequest(request, d_identity, null);

            bool done = false;

            while (!done)
            {
                Event eventObj = d_session.NextEvent();
                if (eventObj.Type == Event.EventType.RESPONSE)
                {
                    fetchedMarketData = processResponseEventAndReturnElement(eventObj,securityDatePair.CUSTOMER,securityDatePair.SEC);
                    done = true;
                }
                else
                {
                    foreach (Message msg in eventObj)
                    {
                        if (eventObj.Type == Event.EventType.SESSION_STATUS)
                        {
                            if (msg.MessageType.Equals("SessionTerminated"))
                                done = true;
                        }
                    }
                }
            }

            return fetchedMarketData;
        }

        private List<SecurityMarketData> processResponseEventAndReturnElement(Event eventObj, int customer = 0, string security = "")
        {
            var retVal = new List<SecurityMarketData>();

            foreach (Message msg in eventObj)
            {
                if (msg.HasElement(RESPONSE_ERROR))
                    continue;
                if ((eventObj.Type != Event.EventType.PARTIAL_RESPONSE) && (eventObj.Type != Event.EventType.RESPONSE))
                    continue;
                if (!ProcessErrors(msg))
                {
                    ProcessExceptions(msg);

                    Element securityData = msg.GetElement(SECURITY_DATA);
                    Element fieldData = securityData.GetElement(FIELD_DATA);

                    //Iterate through all field values returned in the message
                    if (fieldData.NumValues > 0)
                    {
                        for (int j = 0; j < fieldData.NumValues; j++)
                        {
                            Element element = fieldData.GetValueAsElement(j);
                            SecurityMarketData dayPrices = new SecurityMarketData() { CustomerID = customer, Security = security };

                            //Print out the date
                            Datetime date = element.GetElementAsDatetime(DATE);
                            dayPrices.DATE = new DateTime(date.Year,date.Month,date.DayOfMonth);

                            if (element.HasElement("PX_LAST"))
                            {
                                Element temp_field = element.GetElement("PX_LAST");
                                int datatype = temp_field.Datatype.GetHashCode();

                                if (datatype == (int)DataType.FLOAT64)
                                {
                                    Name TEMP_FIELD_STR = new Name("PX_LAST");
                                    var e = element.GetElementAsFloat64(TEMP_FIELD_STR);
                                    dayPrices.PX_LAST = (decimal)e;
                                }
                            }
                            if (element.HasElement("HIGH"))
                            {
                                Element temp_field = element.GetElement("HIGH");
                                int datatype = temp_field.Datatype.GetHashCode();

                                if (datatype == (int)DataType.FLOAT64)
                                {
                                    Name TEMP_FIELD_STR = new Name("HIGH");
                                    var e = element.GetElementAsFloat64(TEMP_FIELD_STR);
                                    dayPrices.HIGH = (decimal)e;
                                }
                            }
                            if (element.HasElement("LOW"))
                            {
                                Element temp_field = element.GetElement("LOW");
                                int datatype = temp_field.Datatype.GetHashCode();

                                if (datatype == (int)DataType.FLOAT64)
                                {
                                    Name TEMP_FIELD_STR = new Name("LOW");
                                    var e = element.GetElementAsFloat64(TEMP_FIELD_STR);
                                    dayPrices.LOW = (decimal)e;
                                }
                            }
                            if (element.HasElement("OPEN"))
                            {
                                Element temp_field = element.GetElement("OPEN");
                                int datatype = temp_field.Datatype.GetHashCode();

                                if (datatype == (int)DataType.FLOAT64)
                                {
                                    Name TEMP_FIELD_STR = new Name("OPEN");
                                    var e = element.GetElementAsFloat64(TEMP_FIELD_STR);
                                    dayPrices.OPEN = (decimal)e;
                                }
                            }
                            if (element.HasElement("PREV_SES_LAST_PRICE"))
                            {
                                Element temp_field = element.GetElement("PREV_SES_LAST_PRICE");
                                int datatype = temp_field.Datatype.GetHashCode();

                                if (datatype == (int)DataType.FLOAT64)
                                {
                                    Name TEMP_FIELD_STR = new Name("PREV_SES_LAST_PRICE");
                                    var e = element.GetElementAsFloat64(TEMP_FIELD_STR);
                                    dayPrices.PREV_SES_LAST_PRICE = (decimal)e;
                                }
                            }
                            if (element.HasElement("LAST_TRADE"))
                            {
                                Element temp_field = element.GetElement("LAST_TRADE");
                                int datatype = temp_field.Datatype.GetHashCode();

                                if (datatype == (int)DataType.FLOAT64)
                                {
                                    Name TEMP_FIELD_STR = new Name("LAST_TRADE");
                                    var e = element.GetElementAsFloat64(TEMP_FIELD_STR);
                                    dayPrices.LAST_TRADE = (decimal)e;
                                }
                            }

                            retVal.Add(dayPrices);
                        }
                    }
                }
            }
            return retVal;
        }

        public void FetchHistoricData()
        {
            switch (d_identity.SeatType)
            {
                case SeatType.BPS:
                    // send request
                    sendHistoricDataRequest();
                    // wait for events from session.
                    eventLoop();
                    break;
                default:
                    throw new Exception("User is invalid.");
            }
        }

        private void sendHistoricDataRequest()
        {
            if (!d_session.OpenService(REFDATA_SVC))
                throw new Exception("Failed to open service: " + REFDATA_SVC);

            Service fieldInfoService = d_session.GetService(REFDATA_SVC);
            Request request = fieldInfoService.CreateRequest("HistoricalDataRequest");

            // Add securities to request
            Element securities = request.GetElement("securities");
            for (int i = 0; i < d_securities.Count; ++i)
            {
                securities.AppendValue((string)d_securities[i]);
            }

            // Add fields to request
            Element fields = request.GetElement("fields");
            for (int i = 0; i < d_fields.Count; ++i)
            {
                fields.AppendValue((string)d_fields[i]);
            }

            request.Set("startDate", d_startDate);
            request.Set("endDate", d_endDate);
            //request.Set("returnEids", true);
            //request.Set("currency", "USD");
            //request.Set("periodicityAdjustment", "CALENDAR");
            //request.Set("periodicitySelection", "MONTHLY");
            //request.Set("nonTradingDayFillOption", "NON_TRADING_WEEKDAYS");
            //request.Set("nonTradingDayFillMethod", "PREVIOUS_VALUE");

            //System.Console.WriteLine("Sending Request: " + request);
            if (d_authOption != "NONE")
            {
                d_session.SendRequest(request, d_identity, null);
            }
            else
            {
                d_session.SendRequest(request, null);
            }
        }
    }
}
