﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ArrayList = System.Collections.ArrayList;
using Datetime = Bloomberglp.Blpapi.Datetime;
using Event = Bloomberglp.Blpapi.Event;
using Element = Bloomberglp.Blpapi.Element;
using Message = Bloomberglp.Blpapi.Message;
using Name = Bloomberglp.Blpapi.Name;
using Identity = Bloomberglp.Blpapi.Identity;
using CorrelationID = Bloomberglp.Blpapi.CorrelationID;
using EventQueue = Bloomberglp.Blpapi.EventQueue;
using Request = Bloomberglp.Blpapi.Request;
using Service = Bloomberglp.Blpapi.Service;
using Session = Bloomberglp.Blpapi.Session;
using SessionOptions = Bloomberglp.Blpapi.SessionOptions;
using DataType = Bloomberglp.Blpapi.Schema.Datatype;
using SeatType = Bloomberglp.Blpapi.SeatType;
using ScriniumMarketDataBLL;

namespace ScriniumMarketDataFetcher
{
    public partial class MarketDataFetcther
    {
        private const String REFDATA_SVC = "//blp/refdata";
        private const String AUTH_SVC = "//blp/apiauth";

        private static readonly Name FIELD_ID = new Name("fieldId");
        private static readonly Name SECURITY_DATA = new Name("securityData");
        private static readonly Name SECURITY_NAME = new Name("security");
        private static readonly Name FIELD_DATA = new Name("fieldData");
        private static readonly Name DATE = new Name("date");
        private static readonly Name RESPONSE_ERROR = new Name("responseError");
        private static readonly Name SECURITY_ERROR = new Name("securityError");
        private static readonly Name FIELD_EXCEPTIONS = new Name("fieldExceptions");
        private static readonly Name ERROR_INFO = new Name("errorInfo");
        private static readonly Name CATEGORY = new Name("category");
        private static readonly Name MESSAGE = new Name("message");
        private static readonly Name AUTHORIZATION_SUCCESS = Name.GetName("AuthorizationSuccess");
        private static readonly Name AUTHORIZATION_FAILURE = Name.GetName("AuthorizationFailure");
        private static readonly Name TOKEN_SUCCESS = Name.GetName("TokenGenerationSuccess");
        
        private static readonly Name TOKEN_FAILURE = Name.GetName("TokenGenerationFailure");

        private ArrayList d_hosts;
        private int d_port;
        private string d_authOption;
        private string d_dsName;
        private string d_name;
        private string d_token;
        private SessionOptions d_sessionOptions;
        private Session d_session;
        private Identity d_identity;
        private ArrayList d_securities;
        public ArrayList SelectedSecurities {
            get { return d_securities; }
            set { d_securities = value; }
        }
        private ArrayList d_fields;
        public ArrayList SelectedFields {
            get { return d_fields; }
            set { d_fields = value; }
        }

        private string d_startDate;
        private string d_endDate;

        public DateTime SelectedStartDate {
            get { return DateTime.ParseExact(d_startDate, "yyyyMMdd",System.Globalization.CultureInfo.InvariantCulture);  }
            set { d_startDate = value.ToString("yyyyMMdd"); }
        }

        public DateTime SelectedEndDate
        {
            get { return DateTime.ParseExact(d_endDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture); }
            set { d_endDate = value.ToString("yyyyMMdd"); }
        }
        
        private bool isAuthorized;
        public bool IsAuthorized { get { return isAuthorized; } }

        public MarketDataFetcther()
        {
            //TODO: add to configruation
            d_hosts = new ArrayList();
            d_hosts.Add("localhost");
            d_securities = new ArrayList();
            d_fields = new ArrayList();
            d_startDate = "null";
            d_endDate = "null";
            d_port = 8294;
            d_dsName = "";
            d_name = "sc:scrinium";
            d_authOption = "APPLICATION";

            if (!validateParameters(null))
                throw new Exception("Validation of parameters failed");

            if (!createSession())
                throw new Exception("Bloomberg session could not be created");

            if (d_authOption != "NONE")
            {
                // Authenticate user using Generate Token Request 
                if (!GenerateToken(out d_token))
                    throw new Exception("Bloomberg token could not be generated");

                //Authorization : pass Token into authorization request. Returns User handle with user's entitlements info set by server.
                if (!IsBPipeAuthorized(d_token, out d_identity))
                    throw new Exception("BPipeNotAuthorized");
                
                // check if user is BPS user
                switch (d_identity.SeatType)
                {
                    case SeatType.BPS:
                        break;
                    default:
                        throw new Exception("User is invalid.");
                }
            }
        }
        ~MarketDataFetcther() {
            d_session.Stop();
        }

        private bool createSession()
        {
            if (d_session != null) d_session.Stop();

            string authOptions = string.Empty;

            d_sessionOptions = new SessionOptions();

            if (d_authOption == "APPLICATION")
            {
                // Set Application Authentication Option
                authOptions = "AuthenticationMode=APPLICATION_ONLY;";
                authOptions += "ApplicationAuthenticationType=APPNAME_AND_KEY;";
                // ApplicationName is the entry in EMRS.
                authOptions += "ApplicationName=" + d_name;
            }
            else if (d_authOption == "USER_APP")
            {
                // Set User and Application Authentication Option
                authOptions = "AuthenticationMode=USER_AND_APPLICATION;";
                authOptions += "AuthenticationType=OS_LOGON;";
                authOptions += "ApplicationAuthenticationType=APPNAME_AND_KEY;";
                // ApplicationName is the entry in EMRS.
                authOptions += "ApplicationName=" + d_name;
            }
            else if (d_authOption == "USER_DS_APP")
            {
                // Set User and Application Authentication Option
                authOptions = "AuthenticationMode=USER_AND_APPLICATION;";
                authOptions += "AuthenticationType=DIRECTORY_SERVICE;";
                authOptions += "DirSvcPropertyName=" + d_dsName + ";";
                authOptions += "ApplicationAuthenticationType=APPNAME_AND_KEY;";
                // ApplicationName is the entry in EMRS.
                authOptions += "ApplicationName=" + d_name;
            }
            else if (d_authOption == "DIRSVC")
            {
                // Authenticate user using active directory service property
                authOptions = "AuthenticationType=DIRECTORY_SERVICE;";
                authOptions += "DirSvcPropertyName=" + d_dsName;
            }
            else if (d_authOption == "NONE")
            {
                // go nothing
            }
            else
            {
                // Authenticate user using windows/unix login name
                authOptions = "AuthenticationType=OS_LOGON";
            }

            System.Console.WriteLine("Authentication Options = " + authOptions);
            d_sessionOptions.AuthenticationOptions = authOptions;

            SessionOptions.ServerAddress[] servers = new SessionOptions.ServerAddress[d_hosts.Count];
            int index = 0;
            System.Console.WriteLine("Connecting to port " + d_port.ToString() + " on host(s):");
            foreach (string host in d_hosts)
            {
                servers[index] = new SessionOptions.ServerAddress(host, d_port);
                System.Console.WriteLine(host);
                index++;
            }

            // auto restart on disconnect
            d_sessionOptions.ServerAddresses = servers;
            d_sessionOptions.AutoRestartOnDisconnection = true;
            d_sessionOptions.NumStartAttempts = d_hosts.Count;

            d_session = new Session(d_sessionOptions);
            return d_session.Start();
        }

        private bool GenerateToken(out string token)
        {
            bool isTokenSuccess = false;
            bool isRunning = false;

            token = string.Empty;
            CorrelationID tokenReqId = new CorrelationID(99);
            EventQueue tokenEventQueue = new EventQueue();

            d_session.GenerateToken(tokenReqId, tokenEventQueue);

            while (!isRunning)
            {
                Event eventObj = tokenEventQueue.NextEvent();
                if (eventObj.Type == Event.EventType.TOKEN_STATUS)
                {
                    System.Console.WriteLine("processTokenEvents");
                    foreach (Message msg in eventObj)
                    {
                        System.Console.WriteLine(msg.ToString());
                        if (msg.MessageType == TOKEN_SUCCESS)
                        {
                            token = msg.GetElementAsString("token");
                            isTokenSuccess = true;
                            isRunning = true;
                            break;
                        }
                        else if (msg.MessageType == TOKEN_FAILURE)
                        {
                            Console.WriteLine("Received : " + TOKEN_FAILURE.ToString());
                            isRunning = true;
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Error while Token Generation");
                            isRunning = true;
                            break;
                        }
                    }
                }
            }
            return isTokenSuccess;
        }

        private bool IsBPipeAuthorized(string token, out Identity identity)
        {
            isAuthorized = false;
            bool isRunning = true;
            identity = null;

            if (!d_session.OpenService(AUTH_SVC))
            {
                //throw new Exception("Failed to open " + AUTH_SVC);
                return (isAuthorized = false);
            }
            Service authService = d_session.GetService(AUTH_SVC);

            Request authRequest = authService.CreateAuthorizationRequest();

            authRequest.Set("token", token);
            identity = d_session.CreateIdentity();
            EventQueue authEventQueue = new EventQueue();

            d_session.SendAuthorizationRequest(authRequest, identity, authEventQueue, new CorrelationID(1));

            while (isRunning)
            {
                //Processing event
                Event eventObj = authEventQueue.NextEvent();
                if (eventObj.Type == Event.EventType.RESPONSE || eventObj.Type == Event.EventType.REQUEST_STATUS)
                {
                    foreach (Message msg in eventObj)
                    {
                        if (msg.MessageType == AUTHORIZATION_SUCCESS)
                        {
                            //Authorazation succeeded
                            isAuthorized = true;
                            isRunning = false;
                            break;
                        }
                        else if (msg.MessageType == AUTHORIZATION_FAILURE)
                        {
                            System.Console.WriteLine("Authorization FAILED");
                            System.Console.WriteLine(msg);
                            isRunning = false;
                        }
                        else
                        {
                            throw new Exception("Something went wrong: " +  msg);
                        }
                    }
                }
            }
            return isAuthorized;
        }
        
        private bool validateParameters(string[] args)
        {   
            // handle default arguments
            if (d_hosts.Count == 0)
                d_hosts.Add("localhost");

            // check for appliation name
            if ((d_authOption == "APPLICATION" || d_authOption == "USER_APP") && (d_name == ""))
                throw new Exception("Application name cannot be NULL for application authorization.");

            // check for directory service and application names
            if (d_authOption == "USER_DS_APP" && (d_name == "" || d_dsName == ""))
                throw new Exception("Application or DS name cannot be NULL for application authorization.");

            // check for Directory Service name
            if (d_authOption == "DIRSVC" && d_dsName == "")
                throw new Exception("Directory Service property name cannot be NULL for DIRSVC authorization.");

            return true;
        }

        private bool isDateTimeValid(string dateTime, string format)
        {
            DateTime outDateTime;
            System.IFormatProvider formatProvider = new System.Globalization.CultureInfo("en-US", true);
            return DateTime.TryParseExact(dateTime, format, formatProvider,
                System.Globalization.DateTimeStyles.AllowWhiteSpaces, out outDateTime);
        }
    }
}